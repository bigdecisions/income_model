import json
import math
from datetime import datetime
from datetime import date
import datetime
import calendar
from pprint import pprint
import numpy as np
import pandas as pd

##global
global mortality_data
mortality_data= pd.read_csv('lookup_mortality_india.csv')
today=datetime.datetime.now().date()
global first_date

def year_diff(d1, d2):
       if d1 == d2:
           return 0
       year = abs(d1.year - d2.year)
       if d1 > d2:
           year -= abs((d1.month, d1.day) < (d2.month, d2.day))
       else:
           year -= abs((d2.month, d2.day) < (d1.month, d1.day))
       return year

def put(data, filename):
	try:
		jsondata = simplejson.dumps(data, indent=4, skipkeys=True, sort_keys=True)
		fd = open(filename, 'w')
		fd.write(jsondata)
		fd.close()
	except:
		print 'ERROR writing', filename
		pass
#-----common----
def add_months(sourcedate,months):
     month = sourcedate.month - 1 + months
     year = int(sourcedate.year + month / 12 )
     month = month % 12 + 1
     day = min(sourcedate.day,calendar.monthrange(year,month)[1])
     return datetime.date(year,month,day)

###---- family codes------####
def married(age,marriage_age):
	if age>mariage_age:
		return true

# self_mortality = mortality(age, gender)
# spouse_mortality = mortality(age, gender)

# max(self_dob + self_mortality, spouse_dob+spouse_mortality)

def first_day_of_month(d):
	return datetime.date(d.year, d.month, 1)

newdate=first_day_of_month(today)

first_date= newdate

first_date=newdate



#------------income --------------#
with open('new_model_test_cases.json') as data_file:    
    income_data = json.load(data_file)


slry_growth=pd.read_csv('lookup_salary_growth_rate.csv')
for z in range(0,len(income_data)):
	print "This is iteration number" + str(z)
	curr_date=datetime.datetime.strptime(income_data[z]["valued_at"], '%Y-%m-%d').date()
	dob=datetime.datetime.strptime(income_data[z]["dob"], '%Y-%m-%d').date()
	curr_age=year_diff(curr_date,dob)
	print curr_age
	income_output={}
	x_var=False
	old_salary=income_data[z]["current_salary"]
	curr_date=first_day_of_month(curr_date)
	while  not (curr_age > income_data[z]["retirement_age"]):
		susbet_slry_growth=slry_growth.query('age=='+str(curr_age))
		if x_var==False:
			susbet_slry_growth['diff']=abs(susbet_slry_growth['predicted_income']-income_data[z]["current_salary"])
			min_index=susbet_slry_growth['diff'].argmin()
			slry_growth_rate=float(susbet_slry_growth.loc[[min_index]]['growth_rate'])
			print susbet_slry_growth
			percentile=float(susbet_slry_growth.loc[[min_index]]['percentile'])
			x_var=True
		else:
			susbet_slry_growth_percentile=susbet_slry_growth.query('percentile=='+str(percentile))
			slry_growth_rate=float(susbet_slry_growth_percentile['growth_rate'])
		slry_growth_rate_month=pow(1 + slry_growth_rate, 1.0 / 12) - 1
		inflation_monthly=pow(1 + 0.07, 1.0 / 12) - 1
		
		new_salary=old_salary*(inflation_monthly+1)*(slry_growth_rate_month+1)
		print curr_date,old_salary,inflation_monthly,slry_growth_rate_month
		income_output[str(curr_date)]=old_salary
		curr_date=add_months(curr_date,1)
		dob=datetime.datetime.strptime(income_data[z]["dob"], '%Y-%m-%d').date()
		curr_age=year_diff(curr_date,dob)
		old_salary=new_salary
	income_data[z]["incomes"]=income_output
	with open("new_model_test_cases_res.json",'w') as fp:
		json.dump(income_data,fp)

#----------expense-------------#
